# filesender

Utilities to share files through a filesender instance.

## Contents

This folder contains two scripts:
1. [filesender.py](filesender.py), in Python, does the main work,
2. [filesender.sh](filesender.sh), in Bash, provides an alternative interface.

## Origin

The script [filesender.py](filesender.py) was originally downloaded in February 2023 from
<https://filesender.renater.fr/clidownload.php>.

These sources are hosted on a server using hosting [GITLAB](https://gitlab.com).

You can see the activity of the project at
<https://gricad-gitlab.univ-grenoble-alpes.fr/guerquim/filesender>.
There among other things, you can download the latest version of the project files.
You can also access the project through GIT.
```bash
$ git clone https://gricad-gitlab.univ-grenoble-alpes.fr/guerquim/filesender.git
```
The project is currently public.

## Dependencies

The [filesender.py](filesender.py) script relies on external Python modules:
1. `requests`
2. `time`
3. `collections.abc`
4. `hmac`
5. `hashlib`
6. `urllib3`
7. `json`
8. `configparser`

## Usage

A few steps to start with:
1. logging-in your filesender instance
2. in your personal profile, create a secret API key, and
3. download your personal configuration file (<https://your.filesender.instance/clidownload.php?config=1>)
4. place the config file in a dedicated hidden folder
```bash
$ mkdir -p ~/.config/filesender/
$ mv filesender.py.ini ~/.config/filesender/filesender.ini
```
5. finally, in the project's folder
```bash
$ make install
$ filesender.py --help
```

The basic use is
```bash
$ filesender.py -r my.recipients@mail.org -s "the subject of the share" -m "hello, this is the file" path/to/the/file/i/want/to.share
```

## Authors

Matthieu Guerquin-Kern, 2023

## License
For the script [filesender.py](filesender.py), see the copyright(left?)
notice at the beginning of the file.

The rest of this work is distributed under the terms of the Unlicense.

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org>
