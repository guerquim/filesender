#! /bin/bash

# AUTHOR:   Matthieu Guerquin-Kern, 2023
# NAME:     share files via filesender
# DESCRIPTION:  A nice Nautilus script with a GUI to share files via filesender
# REQUIRES: filesender.py, zenity
# LICENSE:  Unlicense (https://unlicense.org)

# The code is adapted from Compress PDF (https://launchpad.net/compress-pdf)
VERSION="0.1"

# Check if Zenity is installed
if ! ZENITY=$(which zenity)
then
    echo "error: FileSender $VERSION needs zenity to run." # maybe we could translate this someday
    exit 1
fi

# Check if filesender.py installed
if ! FS=$(which filesender.py)
then
    $ZENITY --error --title="FileSender $VERSION" --text="Cannot find filesender.py\nGo see https://gricad-gitlab.univ-grenoble-alpes.fr/guerquim/filesender"
    exit 1
fi

# Check if the user has selected any files
# if [ "x$NAUTILUS_SCRIPT_SELECTED_FILE_PATHS" = "x"  -o  "$#" = "0" ] # we double check. Remove the first part if you plan to manually invoke the script

if [ "$#" = "0" ] # removed nautilus specific variable to make the script compatible with other file managers
then
    $ZENITY --error --title="FileSender $VERSION" --text="$error_nofiles"
    exit 1
fi

# Check if we can properly parse the arguments
INPUT=("$@")
N=("$#")
if [ "${#INPUT[@]}" != "$N" ] # comparing the number of arguments the script is given with what it can count
then
    $ZENITY --error --title="FileSender $VERSION" # if we arrive here, there is something very messed
    exit 1
fi

# Everything is OK. We can go on.
method=$($ZENITY --list --title="FileSender $VERSION" --text "Please select a sharing method" --radiolist --column "" --column "Method" TRUE "personal invitations to download" FALSE "anonymous download link" --height 100 --width 300)

if [ "$method" = "anonymous download link" ]
then
    data=$(filesender.py -l -n "$@" | tail -2)
    shareurl=$(head -1 <<<$data)
    expires=$(tail -1 <<<$data)
    $($ZENITY --entry --title="FileSender $VERSION" --text="Here is the URL to share: ($expires)" --entry-text="$shareurl" --width=660)
else

# Ask the user to enter recipients' list
recipients=$($ZENITY --entry --title="FileSender $VERSION" --entry-text="prenom1.nom1@grenoble-inp.org,prenom1.nom1@grenoble-inp.org" --text="Comma-separated list of recipient emails" --width 600)
if [ "$?" != "0"  -o  "x$recipients" = x ]; then exit 1; fi

# Ask the user to enter the subject
subject=$($ZENITY --entry --title="FileSender $VERSION" --entry-text="Feedback on..." --text="Subject")
if [ "$?" != "0"  -o  "x$subject" = x ]; then exit 1; fi

# Ask the user to enter the message
message="$($ZENITY --text-info --editable --title="FileSender "$VERSION" : Message to send" --width 400 --height 400 <<< $(printf "Bonjour\nvoici un retour sur votre travail\n...\nN'hésitez pas à me contacter pour toute question.\nMeilleures salutations,"))"
if [ "$?" != "0"  -o  "x$message" = x ]; then exit 1; fi

# Finally, we share the files
expires=$(filesender.py -n -r "$recipients" -m "$message" -s "$subject" "$@" | tail -1)
$($ZENITY --info --title="FileSender $VERSION" --text="$expires")
fi
