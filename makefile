.PHONY: all install

all: install

install:
	@mkdir -p ~/.local/bin/
	@cp filesender.py ~/.local/bin/
	@chmod +x ~/.local/bin/filesender.py
	@mkdir -p ~/.local/share/nautilus/scripts/
	@cp filesender.sh ~/.local/share/nautilus/scripts/
	@chmod +x ~/.local/share/nautilus/scripts/filesender.sh
